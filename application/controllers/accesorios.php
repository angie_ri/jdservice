<?php
class Accesorios extends CI_Controller {

	public function __construct(){
      parent ::__construct();
      $this->load->library('Pagination');
      $this->load->model('Productos_model');

   }

   function index(){

      $datosC = array(
          'titulo' =>'JD Service');
   	$this->load->view("vistasPag/headPag");
      $this->load->view("vistasPag/botoneraPag",$datosC);
      
      $config['base_url']=base_url().'/accesorios/index';
      $config['total_rows']=$this->Productos_model->mostrar_accesorios();
      $config['per_page']=3;
      $config['num_links']=6;
      $config['first_link']="Primero";
      $config['last_link']="Ultimo";
      $config['next_link']="Siguiente";
      $config['prev_link']="Anterior";

      $config['cur_tag_open']='<b class="actual">';
      $config['cur_tag_clase']='</b>';

      $config['full_tag_open']='<div id="pagina">';//revisar
      $config['full_tag_close']='</div>';
      
      $this->pagination->initialize($config);
      
      $data=array('misAccesorios' =>$this->Productos_model->get_accesorios($config['per_page']),'paginas'=>$this->pagination->create_links());
      
      $this->load->view("vistasPag/contenedorAcc", $data );
      $this->load->view("vistasPag/piePag");

   }
   
  
}


?> 
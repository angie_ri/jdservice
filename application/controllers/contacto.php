<?php
class Contacto extends CI_Controller {

	public function __construct(){

		parent::__construct();
        $datosC = array(
            'titulo' =>'JD Service');

        $this->load->helper(array('url','html','form'));
		$this->load->library('form_validation');
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->view("vistasPag/headPag");
    	$this->load->view("vistasPag/botoneraPag",$datosC);
	}

	function index(){

		 $this->load->view("vistasPag/contenedorCon");	
		 $this->load->view("vistasPag/piePag");
          

	}

	 public function formulario(){
		

	 	$this->form_validation->set_rules('nombre','Nombre','required|trim');//trim limpia los espacios en blanco
	 	$this->form_validation->set_rules('apellido','Apellido','required|trim');
	 	$this->form_validation->set_rules('correo','Email','required|valid_email');
	 	$this->form_validation->set_rules('mensaje','Escribir mensaje,','required');

	 	$this->form_validation->set_message('valid_email','correo electronico invalido');
	 	$this->form_validation->set_message('required',' %s es obligatorio');


	 	$mensajes='Gracias por enviar su mensaje,pronto le responderemos si tiene alguna consulta';
		 
		if ($this->form_validation->run() == FALSE){
			
			$this->index();
		  }else{
		  	$this->enviar();
		  	

		  }      
		  		
	}
			 
	
	public function enviar(){
	   
	   	$this->load->library('email');

	   	//configure email setting


	    //get the form data
	   	$nombre=$this->input->post('nombre');
	   	$apellido=$this->input->post('apellido');
	    $contacto = ($nombre+$apellido);
	    $contacto_email = $this->input->post('correo');
	    $mensaje = $this->input->post('mensaje');

	    $this->email->from($contacto_email, $contacto);
	    $this->email->to('ri.angela07@gmail.com');
	    $this->email->subject('TecMovil');
	    $this->email->message($mensaje);

	    $this->email->send();
	    $mensajes=array(
			'mensaje' =>'Gracias por enviar su mensaje,en breve contestaremos su consulta.');
    	
			$this->load->view("vistasPag/vista_formMensaje",$mensajes);
	   
   }
 
 }
?>
<?php
class Inicio extends CI_Controller {

   function index(){

  
      $datos = array(
         'titulo' => 'JD Service',
         'descripcion' => 'Bienvenido a nuestra página web,acá puedes encontrar muchos productos y servicios, estamos para ayudarte.',
         'descripcion1' =>'Puedes encontrar los dispositivos movíles más populares y con la mejor tecnologia, tenemos una amplia gama de accesorio para celulares,tables, computadoras con los mejores precios.',
         'descripcion2'=>'Ofrecemos nuestro servicio técnico para celulares, tables, computadoras y más dispositivos, trabajamos con responsabilidad y nuestros servivios son garantizados. ',
         'cuerpo' => 'El cuerpo de la página probablemente será un texto muy largo...<p>Con varios párrafos</p>'
      );

      $promos = array(
         'dispositivo1'=>' Promo Moto E2','dispositivo2'=>'Promo J1 Ace','dispositivo3'=>'Promo Moto G1',
         'promo1' =>'El Motorola Moto E (2nd Gen) con pantalla qHD de 4.5 pulgadas, procesador quad-core Snapdragon 200 a 1.2GHz, 8GB de almacenamiento interno y Android 5.0 Lollipop, conservando la cámara de 5 megapixeles,dispositivo libre.' ,'promo2'=>'El Samsung Galaxy J1 Ace tiene una pantalla Super AMOLED WVGA de 4.3 pulgadas, procesador dual-core, cámara trasera de 5 MP y frontal de 2 MP, 512MB de RAM, 4GB de almacenamiento interno, y batería de 1800 mAh, dispositivo libre.','promo3'=>'El Motorola Moto G (Moto G1),cámara trasera de 5 megapixels, cámara frontal de 1.3 megapixels y Android 4.3 Jelly Bean, dispositivo libre. ',
         'aviso'=>' Promoción del mes   ','dispositivo4'=>'Moto G2','promoMes'=>'Aprovecha nuestra oferta del mes Motorola G2, completamente libre, posee una pantalla 720p de 5 pulgadas, procesador Snapdragon 400 quad-core a 1.2GHz, 1GB de RAM, cámara de 8 megapixels trasera y 2 MP al frente, ranura microSD, 8GB o 16GB de almacenamiento y parlantes frontales, más información '
      );

      //llamo a las vistas
      $this->load->view("vistaInicio/head");
      $this->load->view("vistaInicio/botonera",$datos);
      $this->load->view("vistaInicio/carrusel",$datos);
      $this->load->view("vistaInicio/contenedor",$promos);
      $this->load->view("vistaInicio/pie",$datos);

    
  }
  

}
?> 

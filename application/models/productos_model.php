<?php
class Productos_model extends CI_Model {

   function __construct(){
      parent::__construct();
  
   }
	//paginacion dispositivo
   	public function mostrar_dispositivos(){

		return $this->db->get('dispositivos')->num_rows();
	}

	public function get_dispositivos($per_page){

		$datos=$this->db->get('dispositivos',$per_page,$this->uri->segment(3));
		return $datos->result_array();
   	}

   	//paginacion accesorios
    public function mostrar_accesorios(){

        return $this->db->get('accesorios')->num_rows();
   	}
   
   	public function get_accesorios($per_page){

        $datos=$this->db->get('accesorios',$per_page,$this->uri->segment(3));
		return $datos->result_array();
   }

   
   
  
}

?> 

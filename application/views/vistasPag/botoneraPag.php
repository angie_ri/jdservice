 
<body>

 <div class="navbar-wrapper">
    <div class="container">

      <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" ><?php echo $titulo ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo site_url('/Inicio')?>">Inicio</a></li>
            <li><a href="<?php echo site_url('/nosotros')?>">Nosotros</a></li>
            <li><a href="<?php echo site_url('/contacto')?>">Contactános</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Productos<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('/dispositivos')?>">Dispositivos Moviles</a></li>
                <li><a href="<?php echo site_url('/accesorios')?>">Accesorios</a></li>
              </ul>
            </li>
          </ul>
        </div>
     </div>
    </nav>
 </div>
</div>
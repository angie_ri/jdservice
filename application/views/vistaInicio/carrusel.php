
    <!-- Carousel
    ================================================== -->
   <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="<?php echo base_url()?>imagenes/cel.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1><?php echo $titulo ?></h1>
              <p><?php echo $descripcion ?><code></code></p>
              <p><a class="btn btn-lg btn-primary" href="<?php echo site_url('/dispositivos')?>" role="button">Más Información</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="<?php echo base_url()?>imagenes/accesorios.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1><?php echo $titulo ?></h1>
              <p><?php echo $descripcion1 ?></p>
              <p><a class="btn btn-lg btn-primary" href="<?php echo site_url('/accesorios')?>" role="button">ver más </a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="third-slide" src="<?php echo base_url()?>imagenes/reparar2.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1><?php echo $titulo ?></h1>
              <p><?php echo $descripcion2 ?></p>
              <p><a class="btn btn-lg btn-primary" href="<?php echo site_url('/contacto')?>" role="button">Comunicate</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->


    
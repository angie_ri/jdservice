  <!-- Marketing messaging and featurettes
    ================================================== -->
 <!-- Wrap the rest of the page in another container to center all the content. -->
    <div class="contenedorInicio">
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      
      <div class="row">
        <div class="col-lg-4"  >
          <img class="img-circle" src="<?php echo base_url()?>imagenes/dispositivos/motoe.jpg" alt="Generic placeholder image" width="350" height="350">
          <h2><?php echo $dispositivo1?></h2>
          <p><?php echo $promo1?></p>
          <p><a class="btn btn-default" href="<?php echo site_url('/dispositivos')?>" role="button">Ver más &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4" >
          <img class="img-circle" src="<?php echo base_url()?>imagenes/dispositivos/ace.jpg" alt="Generic placeholder image" width="350" height="350" >
          <h2 ><?php echo $dispositivo2?></h2>
          <p><?php echo $promo2?></p>
          <p><a class="btn btn-default" href="<?php echo site_url('/dispositivos')?>" role="button">Ver más &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo base_url()?>imagenes/dispositivos/motog.png" alt="Generic placeholder image" width="350" height="350">
          <h2 class="h2"><?php echo $dispositivo3?></h2>
          <p><?php echo $promo3?></p>
          <p><a class="btn btn-default" href="<?php echo site_url('/dispositivos')?>" role="button">Ver más &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->



      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading" ><?php echo $aviso?><span class="text-muted"><?php echo $dispositivo4?></span></h2>
          <p class="lead"><?php echo $promoMes?><a href="<?php echo site_url('/dispositivos')?>"> aquí.</a></p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" src="<?php echo base_url()?>imagenes/dispositivos/motog2.jpg" alt="Generic placeholder image" width="500" height="500">
        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->
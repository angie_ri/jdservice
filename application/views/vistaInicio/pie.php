<!-- FOOTER -->
      <footer class   = "pie">
        <p class="pull-right"><a href="#">Subir</a></p>
        <div class="redes-social" >
            <ul >
              
              <li><a href="#" target="new" ><img alt="Facebook" width="30px" height="30" src="<?php echo base_url()?>imagenes/fn.png" /></a></li>
              <li><a href="#" target="new" ><img alt="Twitter" height="30px" src="<?php echo base_url()?>imagenes/tn.png" width="30px" /></a></li>
              <li><a href="#" itemprop="sameAs" rel="nofollow"><img alt="Instagram" height="30px" src="<?php echo base_url()?>imagenes/in.png" width="30px" /></a></li>
            </ul>
          </div>
          
        <p>&copy;2016 <?php echo $titulo ?> &middot; <a href="http://arsoftdevelop.dx.am/">ArSoft Develop</a></p>
        
      </footer>

    </div>
    </div>
<!-- jQuery -->
    <script src="<?php echo base_url()?>plantilla/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>plantilla/js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
     <!-- Custom styles for this template -->
    <link href="<?php echo base_url()?>css/carousel.css" rel="stylesheet">

     <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url()?>plantilla/assets/js/vendor/jquery.js"><\/script>')</script>
    
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="<?php echo base_url()?>plantilla/assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url()?>plantilla/assets/js/ie10-viewport-bug-workaround.js"></script>

  </body>
</html>

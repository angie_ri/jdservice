-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-07-2016 a las 03:12:54
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `base_tecmovil`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesorios`
--

CREATE TABLE `accesorios` (
  `id_accesorios` int(5) NOT NULL,
  `nombreAc` varchar(30) NOT NULL,
  `descripcionAc` text,
  `precioAc` varchar(10) DEFAULT NULL,
  `imagenRuta` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accesorios`
--

INSERT INTO `accesorios` (`id_accesorios`, `nombreAc`, `descripcionAc`, `precioAc`, `imagenRuta`) VALUES
(1, 'cargador con Usb', 'Cargador con Usb ,originales .', NULL, 'cargadorUsb.jpg'),
(2, 'Cargador Portátil', 'cargador portátil , varios colores.', NULL, 'cargadorPor.png'),
(3, 'Cargador Universal ', 'Cargador universal genérico', NULL, 'cargadorUni2.jpg'),
(4, 'Cargador Universal', 'Cargador universal digital.', '', 'cargadorUni.jpg'),
(5, 'Auricular BlackBerry', 'Auricular Blackberry manos libres, original.', '', 'auriBlack.jpg'),
(6, 'Auriculares para Iphone', 'auriculares para Iphone,generico.', '', 'auricularesIp.jpg'),
(7, 'Auriculares Motorola', 'auriculares Motorola original manos libres.', '', 'auricularesMo.jpg'),
(8, 'Bateria Samsung ', ' Bateria para celularas samsung Ace, 5830 y otros.', NULL, 'samsungAce.jpg'),
(9, 'Bateria Motorola', 'Bateria Motorola ', NULL, 'batemo2.jpg'),
(10, 'Bateria Moto G2', 'Bateria Moto G2 genéricas', NULL, 'bateMoto.jpg'),
(11, 'Batena Nokia', 'Batena Nokia genérico', NULL, 'batenokia.jpg'),
(12, 'Bateria Lg', 'Bateria Lg genérico', NULL, 'bateriaLg.jpg'),
(13, 'Auriculares varios', 'Auriculares varios ,tenemos variedad de colores.', NULL, 'auricomun.jpg'),
(14, 'Auriculares Vincha Philips', 'Auriculares Vincha Philips originales.', NULL, 'auricVinchaPhi.jpg'),
(15, 'Auricular vincha Philips ', 'Auricular vincha Philips original', NULL, 'auriPhil.jpg'),
(16, 'Auricular Sony ', 'Auricular  vincha Sony, original.', NULL, 'auriSony.jpg'),
(17, 'Mouse', 'Mouse con cable', NULL, 'mouseca.gif'),
(18, 'Mouse Inalambrico', 'Mouse Inalambrico .', NULL, 'mouseIn.jpg'),
(19, 'Teclado Pc', 'Teclado Pc con cable.', NULL, 'tecla1.jpg'),
(20, 'Cable Audio', 'Cable auxiliar audio.', NULL, 'cableAudio.jpg'),
(21, 'Cable Audio y video', 'Cable Audio y video', NULL, 'cableAuVi.jpg'),
(22, 'Cable de Datos', 'Cable de Datos para conexión a internet.', NULL, 'cableDatos.jpg'),
(23, 'Cable para Monitor ', 'Cable para Monitor Pc.', NULL, 'cablemoni.jpg'),
(24, 'Cable Pc', 'Cable para Pc.', NULL, 'cablePc.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos`
--

CREATE TABLE `dispositivos` (
  `id_dispositivos` int(5) NOT NULL,
  `nombreDis` varchar(50) NOT NULL,
  `descripcionDis` text,
  `precioDis` varchar(10) NOT NULL,
  `imagenRuta` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivos`
--

INSERT INTO `dispositivos` (`id_dispositivos`, `nombreDis`, `descripcionDis`, `precioDis`, `imagenRuta`) VALUES
(1, 'J1Ace', 'El Samsung Galaxy J1 Ace tiene una pantalla Super AMOLED WVGA de 4.3 pulgadas, procesador dual-core, cámara trasera de 5 MP y frontal de 2 MP, 512MB de RAM, 4GB de almacenamiento interno, y batería de 1800 mAh, dispositivo libre.', '$000', 'ace.jpg'),
(2, 'Moto G1', 'El Motorola Moto G (Moto G1),cámara trasera de 5 megapixels, cámara frontal de 1.3 megapixels y Android 4.3 Jelly Bean, dispositivo libre. ', '$000', 'motog.png'),
(3, 'Moto G2', 'Motorola G2, completamente libre, posee una pantalla 720p de 5 pulgadas, procesador Snapdragon 400 quad-core a 1.2GHz, 1GB de RAM, cámara de 8 megapixels trasera y 2 MP al frente, ranura microSD, 8GB o 16GB de almacenamiento y parlantes frontales.', '', 'motog2.jpg'),
(4, 'Moto E2', 'El Motorola Moto E (2nd Gen) con pantalla qHD de 4.5 pulgadas, procesador quad-core Snapdragon 200 a 1.2GHz, 8GB de almacenamiento interno y Android 5.0 Lollipop, conservando la cámara de 5 megapixeles,dispositivo libre.', '$000', 'motoe.jpg'),
(5, 'Samsung Galaxy Ace 4', 'El Galaxy Ace 4 está equipado con un procesador de 1 GHz, pantalla de 4", batería de 1,500 mAh e interfaz de usuario TouchWiz Essence UX,\r\n pantalla WVGA de 4 pulgadas, 4GB de RAM, cámara de 5 MP - pero posee procesador dual-core a 1.2GHz, 1GB de RAM.', '', 'ace 4.jpg'),
(6, 'Samsung Galaxy Grand 2', '.El Samsung Galaxy Grand 2 es el sucesor del Galaxy Grand original, con una pantalla más grande y de mayor resolución - 5.25 pulgadas - procesador quad-core, 1.5GB de RAM, cámara de 8 megapixels con flash LED, 8GB de almacenamiento interno, soporte SIM dual y Android 4.3 Jelly Bean', '', 'samGran.png'),
(7, 'Samsung Galaxy Core', 'El Samsung Galaxy Core es un smartphone Android de gama media, con una pantalla WVGA de 4.3 pulgadas y por lo tanto baja densidad de pixel, procesador dual-core a 1.2GHz, cámara trasera de 5 megapixels con flash LED, cámara frontal para video chat, 1GB de RAM, 8GB de almacenamiento interno, ranura microSD y corre Android 4.1 Jelly Bean.', '', 'core2.jpg'),
(8, 'Samsung E1205 ', 'El Samsung E1205 es un sencillo teléfono celular GSM. Posee una pantalla color de 2 pulgadas.', '', 'sam1205.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accesorios`
--
ALTER TABLE `accesorios`
  ADD PRIMARY KEY (`id_accesorios`);

--
-- Indices de la tabla `dispositivos`
--
ALTER TABLE `dispositivos`
  ADD PRIMARY KEY (`id_dispositivos`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accesorios`
--
ALTER TABLE `accesorios`
  MODIFY `id_accesorios` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
